window.addEventListener("load", function() {
    var vanillaModal = new VanillaModal.default({
        onBeforeOpen: function(event) {

            var element = event.target,
                tag = element.tagName.toUpperCase(),
                find = tag == "A";
            while (!find) {
                var element = element.parentElement,
                    parentTag = element.tagName.toUpperCase();
                if (!parent || parentTag == "BODY") {
                    break;
                } else if (parentTag == "A") {
                    find = true;
                    break;
                }
            }
            if (find) {
                var iframeEl = element.getAttribute("data-iframe"),
                    id = element.href.substr(
                        element.href.lastIndexOf('#') + 1
                    );
                if (typeof iframeEl == "string") {
                    var targ = document.getElementById(id);
                    if (targ) {
                        var iframeOne = targ.getElementsByTagName("iframe");
                        console.log(iframeOne);
                        if (iframeOne) {
                            iframeOne[0].setAttribute('src', iframeEl);
                        }
                    }
                }
            }
        }
    });
});